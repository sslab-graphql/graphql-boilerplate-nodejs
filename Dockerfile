FROM node:14-alpine
WORKDIR /app

COPY package.json yarn.lock ./

RUN yarn install --pure-lockfile
COPY . .

EXPOSE 4000

ENTRYPOINT ["yarn", "start"]